package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class CreditUnion extends Finance implements Credit {

    private double interestRate;
    private int maxCredit;

    public int getMaxCredit() {
        return maxCredit;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public CreditUnion(String name, String address, double interestRate, int maxCredit) {
        super(name, address);
        this.interestRate = interestRate;
        this.maxCredit = maxCredit;
    }

    @Override
    public double getCredit(int uah) {
        return uah + uah * interestRate;
    }

    @Override
    public boolean couldGiveCredit(int amountOfCredit) {
        return amountOfCredit <= maxCredit;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", interest rate: " + interestRate + ", max credit: " + maxCredit;
    }
}
