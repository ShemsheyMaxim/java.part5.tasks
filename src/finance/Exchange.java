package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public interface Exchange {
    double convertUAHinUSD(int uah);

    double convertUAHinEUR(int uah);

    double convertUSD(int usd);

    double convertEUR(int eur);

    boolean couldExchangeAmountUAH(int amountUAH);
}
