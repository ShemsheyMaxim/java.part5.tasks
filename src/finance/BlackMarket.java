package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class BlackMarket extends Finance implements Exchange {

    private double usd;
    private double eur;

    public double getUsd() {
        return usd;
    }

    public double getEur() {
        return eur;
    }

    public BlackMarket(String name, String address, double usd, double eur) {
        super(name, address);
        this.usd = usd;
        this.eur = eur;
    }

    @Override
    public double convertUAHinUSD(int uah) {
        return uah / usd;
    }

    @Override
    public double convertUAHinEUR(int uah) {
        return uah / eur;
    }

    @Override
    public double convertUSD(int usd) {
        return usd * this.usd;
    }

    @Override
    public double convertEUR(int eur) {
        return eur * this.eur;
    }

    @Override
    public boolean couldExchangeAmountUAH(int amountUAH) {
        return true;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", course USD: " + usd + ", course EUR: " + eur;
    }
}
