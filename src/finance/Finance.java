package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class Finance {

    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Finance(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getInfo() {
        return name + ", " + address;
    }
}
