package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class MIF extends Finance implements Deposit {

    private int yearOfFoundation;
    private double depositPercentage;

    public int getYearOfFoundation() {
        return yearOfFoundation;
    }

    public double getDepositPercentage() {
        return depositPercentage;
    }

    public MIF(String name, String address, int yearOfFoundation, double depositPercentage) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
        this.depositPercentage = depositPercentage;
    }

    @Override
    public double getDeposit(int uah) {
        return uah + uah * depositPercentage;
    }

    @Override
    public boolean couldGetDeposit(int timeDeposit) {
        return timeDeposit >= 12;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", year of foundation: " + yearOfFoundation + ", deposit percentage: " + depositPercentage;
    }
}
