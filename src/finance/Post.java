package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class Post extends Finance implements Remittance {

    private double commission;

    public double getCommission() {
        return commission;
    }

    public Post(String name, String address, double commission) {
        super(name, address);
        this.commission = commission;
    }

    @Override
    public double getRemittance(double uah) {
        return uah - (uah * commission);
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", commission:" + commission;
    }
}
