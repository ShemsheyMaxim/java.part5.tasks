package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public class Bank extends Finance implements Exchange, Credit, Deposit, Remittance {

    private int yearOfLicense;
    private double usd;
    private double eur;
    private double interestRate;
    private int maxCredit;
    private double depositPercentage;
    private double commission;
    private int additionalInterestForCommission;

    public int getYearOfLicense() {
        return yearOfLicense;
    }

    public double getUsd() {
        return usd;
    }

    public double getEur() {
        return eur;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public int getMaxCredit() {
        return maxCredit;
    }

    public double getDepositPercentage() {
        return depositPercentage;
    }

    public double getCommission() {
        return commission;
    }

    public int getAdditionalInterestForCommission() {
        return additionalInterestForCommission;
    }

    public Bank(String name, String address, int yearOfLicense, double usd, double eur, double interestRate, int maxCredit, double depositPercentage, double commission, int additionalInterestForCommission) {
        super(name, address);
        this.yearOfLicense = yearOfLicense;
        this.usd = usd;
        this.eur = eur;
        this.interestRate = interestRate;
        this.maxCredit = maxCredit;
        this.depositPercentage = depositPercentage;
        this.commission = commission;
        this.additionalInterestForCommission = additionalInterestForCommission;
    }

    @Override
    public double getCredit(int uah) {
        return uah + uah * interestRate;
    }

    @Override
    public boolean couldGiveCredit(int amountOfCredit) {
        return amountOfCredit <= maxCredit;
    }

    @Override
    public double getDeposit(int uah) {
        return uah + uah * depositPercentage;
    }

    @Override
    public boolean couldGetDeposit(int timeDeposit) {
        return timeDeposit <= 12;
    }

    @Override
    public double convertUAHinUSD(int uah) {
        return (uah - 15) / usd;
    }

    @Override
    public double convertUAHinEUR(int uah) {
        return (uah - 15) / eur;
    }

    @Override
    public double convertUSD(int usd) {
        return usd * this.usd;
    }

    @Override
    public double convertEUR(int eur) {
        return eur * this.eur;
    }

    @Override
    public boolean couldExchangeAmountUAH(int amountUAH) {
        return amountUAH <= 12_000;
    }

    @Override
    public double getRemittance(double uah) {
        return uah - (uah * commission + additionalInterestForCommission);
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", " + getYearOfLicense() + ", course USD: " + usd + ", course EUR: " + eur + ", interest rate: " + interestRate + ", max credit: " + maxCredit + ", deposit percentage: " + depositPercentage + ", commission: " + commission + ", additional interest for commission: " + additionalInterestForCommission;
    }
}
