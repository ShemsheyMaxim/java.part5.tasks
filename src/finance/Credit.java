package finance;

/**
 * Created by Максим on 24.03.2017.
 */
public interface Credit {
    double getCredit(int uah);

    boolean couldGiveCredit(int amountOfCredit);
}
